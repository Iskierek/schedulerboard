﻿using Microsoft.EntityFrameworkCore;
using SchedulerBoard.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchedulerBoard.Data
{
    public class SchedulerDbContext : DbContext
    {

        public SchedulerDbContext(DbContextOptions<SchedulerDbContext> options)
           : base(options)
        {
        }
        public DbSet<Event> Event { get; set; }
        public DbSet<Group> Group { get; set; }
        public DbSet<Player> Player { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        
            {


                modelBuilder.Entity<Event>().ToTable(nameof(Event))
                            .HasMany<Player>(s => s.Players)
                            .WithMany(c => c.Events);






        }
    }
}
