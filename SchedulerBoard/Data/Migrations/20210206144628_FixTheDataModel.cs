﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SchedulerBoard.Data.Migrations
{
    public partial class FixTheDataModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Event_Group_GroupId",
                table: "Event");

            migrationBuilder.DropIndex(
                name: "IX_Event_GroupId",
                table: "Event");

            migrationBuilder.AddColumn<int>(
                name: "Group",
                table: "Event",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Event_Group",
                table: "Event",
                column: "Group");

            migrationBuilder.AddForeignKey(
                name: "FK_Event_Group_Group",
                table: "Event",
                column: "Group",
                principalTable: "Group",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Event_Group_Group",
                table: "Event");

            migrationBuilder.DropIndex(
                name: "IX_Event_Group",
                table: "Event");

            migrationBuilder.DropColumn(
                name: "Group",
                table: "Event");

            migrationBuilder.CreateIndex(
                name: "IX_Event_GroupId",
                table: "Event",
                column: "GroupId");

            migrationBuilder.AddForeignKey(
                name: "FK_Event_Group_GroupId",
                table: "Event",
                column: "GroupId",
                principalTable: "Group",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
