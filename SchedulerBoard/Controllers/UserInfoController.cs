﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SchedulerBoard.Areas.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace SchedulerBoard.Controllers
{
    public class UserInfoController : Controller
    {



        [Authorize]
        public IActionResult Index()
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var userName = User.FindFirstValue(ClaimTypes.Name); 
            var userEmail = User.FindFirstValue(ClaimTypes.Email); 
            ViewData["UserIDentityName"] = "UserIdentityName " + User.Identity.Name;
            ViewData["userId"] = "UserId " + userId;
            ViewData["userName"] = "userName " + userName;
            ViewData["userEmail"] = "userEmail " + userEmail;
            return View();
        }

        public string yolo() {
            return "yolo?";
        }
    }
}
