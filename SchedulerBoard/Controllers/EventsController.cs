﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SchedulerBoard.Data;
using SchedulerBoard.Models;

namespace SchedulerBoard.Controllers
{
    public class EventsController : Controller
    {
        private readonly SchedulerDbContext _context;

        public EventsController(SchedulerDbContext context)
        {
            _context = context;
        }

        // GET: Events
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.Event.Include(p => p.group);

            return View(await applicationDbContext.ToListAsync());
        }

        // GET: Events/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var @event = await _context.Event
                .Include(p => p.group)
                .ThenInclude(f => f.players)
                .Include(e => e.Players)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (@event == null)
            {
                return NotFound();
            }

            return View(@event);
        }

        // GET: Events/Create
        public IActionResult Create()
        {
            ViewData["GroupId"] = new SelectList(_context.Group, "Id", "Id");
            return View();
        }

        // POST: Events/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,GroupId,Title,EventDate")] Event @event)
        {
            if (ModelState.IsValid)
            {
                _context.Add(@event);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["GroupId"] = new SelectList(_context.Group, "Id", "Id", @event.GroupId);
            return View(@event);
        }

        // GET: Events/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var @event = await _context.Event
                .Include(p => p.group)
                .ThenInclude(f => f.players)
                .Include(e => e.Players)
                .FirstOrDefaultAsync(m => m.Id == id);

            var newEventModel = new EventViewModel();
            newEventModel.Title = @event.Title;
            newEventModel.EventDate = @event.EventDate;
            newEventModel.GroupId = @event.GroupId;

            foreach (var player in @event.group.players) {
                var newEventPlayer = new EventPlayersViewModel();
                newEventPlayer.Name = player.Nick;
                newEventPlayer.PlayerId = player.Id;
                if (@event.Players.Contains(player)) {
                    newEventPlayer.IsChecked = true;
                }
                newEventModel.EventPlayers.Add(newEventPlayer);
            }

            if (@event == null)
            {
                return NotFound();
            }
            ViewData["GroupId"] = new SelectList(_context.Group, "Id", "Id", @event.GroupId);
            return View(newEventModel);
        }

        // POST: Events/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, EventViewModel @event)
        {
            if (id != @event.Id)
            {
                return NotFound();
           }



            var eventToUpdate = await _context.Event
                .Include(p => p.group)
                .ThenInclude(f => f.players)
                .Include(e => e.Players)
                .FirstOrDefaultAsync(m => m.Id == id);


            eventToUpdate.Title = @event.Title;
            eventToUpdate.GroupId = @event.GroupId;
            eventToUpdate.EventDate = @event.EventDate;
            eventToUpdate.Players.Clear();
            

            foreach (var player in @event.EventPlayers.Where(x=> x.IsChecked)){
            
                var newPlayer= eventToUpdate.group.players.Where(x=>x.Id==player.PlayerId).FirstOrDefault();
                eventToUpdate.Players.Add(newPlayer);

            }



            if (ModelState.IsValid)
            {
                try
                {
                

                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EventExists(@event.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["GroupId"] = new SelectList(_context.Group, "Id", "Id", @event.GroupId);
            return View(@event);
        }

        // GET: Events/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var @event = await _context.Event
                .Include(p => p.group)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (@event == null)
            {
                return NotFound();
            }

            return View(@event);
        }

        // POST: Events/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var @event = await _context.Event.FindAsync(id);
            _context.Event.Remove(@event);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EventExists(int id)
        {
            return _context.Event.Any(e => e.Id == id);
        }

        public void UpdateEventPlayers(string[] selectedPlayers,
                                               Event eventToUpdate)
        {
            if (selectedPlayers == null)
            {

                return;
            }

            var selectedPlayersHS = new HashSet<string>(selectedPlayers);
            var PlayerEvent = new HashSet<int>
                (eventToUpdate.Players.Select(c => c.Id));

        }
    } }
