﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SchedulerBoard.Data;
using SchedulerBoard.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace SchedulerBoard.Controllers
{
    public class BggCollectionController : Controller
    {
        // GET: BggCollectionController
        private readonly SchedulerDbContext _context;

        public BggCollectionController(SchedulerDbContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index()
        {


            var Bggcollection = await _context.Player.Include(p => p.group).ToListAsync();
          


            var BggViewModel = new BggCollectionViewModel();

            BggViewModel.players = Bggcollection;
            BggViewModel.BggUserNameList = Bggcollection.Select(k => k.BggUserName).ToList();


            //XML COLLECTION
            var urlLink = "https://boardgamegeek.com/xmlapi/collection/Iskierek?preordered=0";
            var XmlfromStream = testGetXmlDocFromWebRequest(urlLink);
            var XmlFromWebReqeust = SendXmlRequest(urlLink);


            return View(BggViewModel);
        }

        static XDocument testGetXmlDocFromWebRequest(string url)
        {
            string response = string.Empty;
            WebRequest request = WebRequest.Create(url);
            try
            {

                using (StreamReader streamIn = new StreamReader((request.GetResponse()).GetResponseStream()))
                {
                   
                    response = streamIn.ReadToEnd();
                    
                    streamIn.Close();
                }
            }
            finally
            {
                request.Abort();

            }
            return XDocument.Parse(response);
        }


        public String SendXmlRequest(string Url)
        {
            var request = WebRequest.Create(Url) as HttpWebRequest;
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            var responsecode = response.StatusCode; //Accepted is the one we want to catch
            var responsestatus = response.StatusDescription;
            Stream receiveStream = response.GetResponseStream();
         
            StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);

            return readStream.ReadToEnd();

        }

    }
}
