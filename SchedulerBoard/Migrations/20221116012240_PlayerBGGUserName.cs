﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SchedulerBoard.Migrations
{
    public partial class PlayerBGGUserName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "BggUserName",
                table: "Player",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BggUserName",
                table: "Player");
        }
    }
}
