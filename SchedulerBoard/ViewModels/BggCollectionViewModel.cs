﻿
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SchedulerBoard.Data;
using SchedulerBoard.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SchedulerBoard.ViewModels
{
    public class BggCollectionViewModel
    {


        public IEnumerable<Player> players { get; set; }

        public List<String> BggUserNameList { get; set; }

        public SelectList GroupNamesSelectItems { get; set; }

      

    }
}
