﻿
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SchedulerBoard.Data;
using SchedulerBoard.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SchedulerBoard.ViewModels
{
    public class GroupViewModel
    {

        public int Id { get; set; }

        public int GroupId { get; set; }
        public string Title { get; set; }

        public IEnumerable<Player> players { get; set; }
        public IEnumerable<Event> events { get; set; }

        public IEnumerable<SelectListItem> GroupList { get; set; }
        public SelectList GroupNamesSelectItems { get; set; }

      

    }
}
