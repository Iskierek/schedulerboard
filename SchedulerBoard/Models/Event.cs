﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SchedulerBoard.Models
{
    public class Event
    {
        public int Id { get; set; }
        [ForeignKey("Group")]
        public int GroupId { get; set; }
        public string Title { get; set; }

        [DataType(DataType.Date)]
        public DateTime EventDate { get; set; }


        public virtual ICollection<Player> Players { get; set; }

        public Group group { get; set; }
    }
}
