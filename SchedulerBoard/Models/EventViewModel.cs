﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace SchedulerBoard.Models
{
    public class EventViewModel
    {

        public int Id { get; set; }

        public int GroupId { get; set; }
        public string Title { get; set; }

        public DateTime EventDate { get; set; }

        public List<EventPlayersViewModel> EventPlayers { get; set; } = new List<EventPlayersViewModel>();
        
    }


    public class EventPlayersViewModel
    {
        public int PlayerId { get; set; }
        public string Name { get; set; }
        public bool IsChecked { get; set; }
    }
}


