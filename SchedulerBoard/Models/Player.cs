﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SchedulerBoard.Models
{
    public class Player
    {
        public int Id { get; set; }
        [ForeignKey("Group")]
        public int GroupId { get; set; }
        public string Nick { get; set; }

        public string BggUserName { get; set; }

        public Group group { get; set; }

        public virtual ICollection<Event> Events { get; set; }

    }
}
