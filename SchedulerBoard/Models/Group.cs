﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchedulerBoard.Models
{
    public class Group
    {
        public int Id { get; set; }
        public string Title { get; set; }

        public IEnumerable<Player> players { get; set; }
        public IEnumerable<Event> events { get; set; }

        
     

    }
}
